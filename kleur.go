package main

import (
	"fmt"
	//"os"
)

var input string
var colors = [4]string{"blue", "red", "yellow", "green"}
//var quotes = []
func main () {
	fmt.Println("Write a color")
	fmt.Scanln(&input)

	if input == "blue" {
		fmt.Println("blue as the sky")
		return
	} 
	if input == "red" {
		fmt.Println("red with passion")
		return
	} 
	if input == "yellow" {
		fmt.Println("yellow as the sunsine")
		return
	} 
	if input == "green" {
		fmt.Println("green of the nature")
		return
	} 
	
	if input != "colors" {
		fmt.Println("error")
	}
}